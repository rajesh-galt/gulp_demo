# README #

This is a sample project to get started with the gulp automation tool.

Using this project, It automatically compiles the scss files to css and then minifies the css files and javascript files.

It also minifies the images.

We can run the project using the command "gulp watch" which will open the webpage automatically in the browser. As you make any changes to the files, the browser automatically reloads with the changes made.